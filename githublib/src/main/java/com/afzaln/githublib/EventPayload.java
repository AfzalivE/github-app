package com.afzaln.githublib;

import java.util.List;

import com.afzaln.githublib.Event.Repo;

/**
 * Created by afzal on 15-02-04.
 */
public class EventPayload {

    public static class IssuesEvent extends EventPayload {
        public String action;
        public Issue issue;
//        public User assignee;
        public Label label;
    }

    public static class IssueCommentEvent extends EventPayload {
        public String action;
        public Issue issue;
        public IssueComment comment;
    }

    public static class PushEvent extends EventPayload {
        public String pushId;
        public int size;
        public String ref;
        public String head;
        public String before;
        public List<PushCommit> commits;

        /**
         * Commit in a push
         */
        public static class PushCommit {
            public String sha;
            public Author author;
            public String message;
            public String url;
            public boolean distinct;
        }

        public static class Author {
            public String name;
            public String email;
            public String username;

        }
    }

    public static class ForkEvent extends EventPayload {
        public Repo forkee;
    }

    public static class PullRequestEvent extends EventPayload {
        public String action;
        public int number;
        public PullRequest pullRequest;
    }

    public static class CreateEvent extends EventPayload {
        public String refType;
        public String ref;
        public String masterBranch;
        public String description;
    }

    public static class DeleteEvent extends EventPayload {
        public String refType;
        public String ref;
    }
}
