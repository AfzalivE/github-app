package com.afzaln.githublib;

/**
 * Created by afzal on 15-02-02.
 */
public interface LoginStateListener {
    public void onLoginStateChanged(LoginState newState);
}
