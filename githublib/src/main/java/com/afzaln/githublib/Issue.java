package com.afzaln.githublib;

import java.util.List;

/**
* Created by afzal on 15-02-04.
*/
public class Issue {
    public User assignee;
    public String state;
    public int number;
    public String closed_at;
    public int comments;
    public String body;
    public List<Label> labels;
    public User user;
    public String title, created_at, html_url;
    public PullRequest pull_request;
    public Milestone milestone;
    public String url, updated_at;
    public int id;
    public User closed_by;

}
