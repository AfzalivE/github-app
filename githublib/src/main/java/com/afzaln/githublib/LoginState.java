package com.afzaln.githublib;

/**
 * Created by afzal on 15-02-02.
 */
public enum LoginState {
    ANONYMOUS,
    BAD_CREDENTIALS,
    NEEDS_TWO_FA,
    MAX_ATTEMPTS_EXCEEDED,
    TOKEN_CREATED,
    LOGGED_IN_WITH_TOKEN,
    LOGGED_IN
}
