package com.afzaln.githublib;

import java.util.Date;
import java.util.List;

import com.afzaln.githublib.gson.AnnotationExclusionStrategy;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.internal.bind.DateTypeAdapter;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Created by afzal on 15-02-02.
 */
public class NetUtils {
    private static final String API_ENDPOINT = "https://api.github.com";

    static RestAdapter buildNetService(RequestInterceptor requestInterceptor) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_ENDPOINT)
                .setLogLevel(LogLevel.FULL)
                .setRequestInterceptor(requestInterceptor)
                .setConverter(new GsonConverter(sGson))
                .build();

        return restAdapter;
    }

    static boolean checkHeaderExists(Response response, String headerName) {
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName() != null && header.getName().equals(headerName)) {
                return true;
            }
        }
        return false;
    }

    static String fetchMessage(RetrofitError error) {
        JsonObject responseJson = (JsonObject) error.getBodyAs(JsonObject.class);
        String message = responseJson.get("message").getAsString();
        return message;
    }

    public static Gson sGson = new GsonBuilder()
            .setExclusionStrategies(new AnnotationExclusionStrategy())
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .registerTypeAdapter(Date.class, new DateTypeAdapter())
            .create();
}
