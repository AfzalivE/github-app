package com.afzaln.githublib;

import android.content.Context;

/**
 * Created by afzal on 15-02-02.
 */
public class GithubBuilder {

    private String login;
    private String password;
    private LoginStateListener loginStateListener;
    private Context context;
    private GHAuthorization authorization;

    public GithubBuilder() {}

    public GithubBuilder withPassword(Context context, String login, String password, LoginStateListener loginStateListener) {
        this.context = context;
        this.login = login;
        this.password = password;
        this.loginStateListener = loginStateListener;
        return this;
    }

    public GithubBuilder withOauthToken(Context context, LoginStateListener loginStateListener) {
        this.context = context;
        this.loginStateListener = loginStateListener;
        this.authorization = GHAuthorization.fetch(context);
        return this;
    }

    public Github build() {
        if (authorization != null) {
            return new Github(context, authorization, loginStateListener);
        } else {
            return new Github(context, login, password, loginStateListener);
        }
    }
}
