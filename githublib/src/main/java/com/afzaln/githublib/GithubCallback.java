package com.afzaln.githublib;

import org.apache.http.HttpStatus;

import android.util.Log;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by afzal on 15-02-02.
 */
public abstract class GithubCallback<T> implements Callback<T> {
    private static String TAG;

    private final Github mGithub;

    public GithubCallback(Github github) {
        TAG = this.getClass().getSimpleName();
        mGithub = github;
    }

    @Override
    abstract public void success(T t, Response response);

    @Override
    public void failure(RetrofitError error) {
        // check if 401, if X-Github-OTP header is set
        Response response = error.getResponse();

        switch (response.getStatus()) {
            case HttpStatus.SC_BAD_REQUEST:
                if (NetUtils.fetchMessage(error).equals("Problems parsing JSON")) {
                    // TODO app error, formed bad json, check correct flow later
                    // happens with the request json is bad
                    throw new IllegalStateException(error);
                }
                break;
            case HttpStatus.SC_UNPROCESSABLE_ENTITY:
                if (NetUtils.fetchMessage(error).equals("Validation Failed")) {
                    // TODO app error, happens when a required field is missing
                    throw new IllegalStateException(error);
                } else if (NetUtils.fetchMessage(error).equals("Invalid OAuth application client_id or secret.")) {
                    // happens when one of client id or secret is missing (but not both)
                    // both missing creates a token but doesn't associate with the app
                    throw new IllegalStateException(error);
                }
                break;
            case HttpStatus.SC_UNAUTHORIZED:
                if (NetUtils.fetchMessage(error).equals("Bad credentials")) {
                    Log.d(TAG, "bad username or password");
                    mGithub.updateLoginState(LoginState.BAD_CREDENTIALS);
                } else if (NetUtils.fetchMessage(error).equals("Requires authentication")) {
                    // TODO app error
                    // happens when there's no Authorization header in the request
                    throw new IllegalStateException(error);
                } else {
                    if (NetUtils.checkHeaderExists(response, "X-GitHub-OTP")) {
                        Log.d(TAG, "need two factor auth");
                        mGithub.updateLoginState(LoginState.NEEDS_TWO_FA);
                    }
                }
                break;
            case HttpStatus.SC_FORBIDDEN:
                String message = NetUtils.fetchMessage(error);
                if (message.startsWith("Maximum number of login attempts exceeded")) {
                    Log.d(TAG, message);
                    mGithub.updateLoginState(LoginState.MAX_ATTEMPTS_EXCEEDED);
                }
                break;
        }
    }
}
