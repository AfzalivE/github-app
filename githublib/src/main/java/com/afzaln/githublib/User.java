package com.afzaln.githublib;

/**
 * Created by afzal on 15-01-31.
 */
public class User {
    public String login;
    public long id;
    public String avatarUrl;
    public String gravatarId;
    public String url;
    public String htmlUrl;
    public String followersUrl;
    public String followingUrl;
    public String gistsUrl;
    public String starredUrl;
    public String subscriptionsUrl;
    public String organizationsUrl;
    public String reposUrl;
    public String eventsUrl;
    public String receivedEventsUrl;
    public String type;
    public String siteAdmin;
    public String publicRepos;
    public String publicGists;
    public String followers;
    public String following;
    public String createdAt;
    public String updatedAt;
    public String privateGists;
    public String totalPrivateRepos;
    public String ownedPrivateRepos;
    public String diskUsage;
}
