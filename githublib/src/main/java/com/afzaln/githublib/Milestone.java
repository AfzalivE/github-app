package com.afzaln.githublib;

/**
 * Created by afzal on 15-02-04.
 */
public class Milestone {
    String url;
    int number;
    String state;
    String title;
    String description;
    User creator;
    int openIssues;
    int closedIssues;
    String createdAt;
    String updatedAt;
    String closedAt;
    String dueOn;
}
