package com.afzaln.githublib;

import java.util.List;

import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by afzal on 15-02-04.
 */
public class EventsApi {

    private static final String TAG = EventsApi.class.getSimpleName();

    private Github mGithub;

    /**
     * Callback declarations, not initialized because
     * they need a non-null mGithub reference in constructor
     */
    private GithubCallback<List<Event>> receivedEventsCallback;

    EventsApi(Github github) {
        mGithub = github;
        initCallbacks();
    }

    interface EventService {

        @GET("/users/{username}/received_events")
        void receivedEvents(@Path("username") String username, GithubCallback<List<Event>> callback);
    }

    public void receivedEvents(GithubCallback<List<Event>> receivedEventsCallback) {
        EventService service = buildNetService();
        service.receivedEvents(mGithub.currentUser.login, receivedEventsCallback);
    }

    private EventService buildNetService() {
        RequestInterceptor interceptor = createRequestInterceptor();
        RestAdapter adapter = NetUtils.buildNetService(interceptor);
        return adapter.create(EventService.class);
    }

    private RequestInterceptor createRequestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                if (mGithub.authorization != null) {
                    request.addHeader("Authorization", "token " + mGithub.authorization.token);
                } else {
                    throw new IllegalStateException("No authorization token stored, create one using AuthApi first");
                }
            }
        };
    }

    /**
     * Initialize all callbacks
     */
    private void initCallbacks() {
        receivedEventsCallback = new GithubCallback<List<Event>>(mGithub) {
            @Override
            public void success(List<Event> events, Response response) {
                Log.d(TAG, "Events size: " + events.size());
            }
        };
    }
}
