package com.afzaln.githublib;

import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.afzaln.githublib.gson.Exclude;
import com.google.gson.Gson;
import com.securepreferences.SecurePreferences;

/**
 * Created by afzal on 15-02-02.
 */
public class GHAuthorization {
    @Exclude
    Github mGithub;
    int id;
    String url;
    List<String> scopes;
    String token;
    String tokenLastEight;
    String hashedToken;
    App app;
    String note;
    String noteUrl;
    String updatedAt;
    String createdAt;
    String clientId;
    String clientSecret;

    public GHAuthorization(Github github, String scopes, String note, String noteUrl, String clientId, String clientSecret) {
        mGithub = github;
        this.scopes = Arrays.asList(scopes.split(","));
        this.note = note;
        this.noteUrl = noteUrl;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    public void authorize() {
        new AuthApi(mGithub).createToken(this);
    }

    public String serialize() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public void save(Context context) {
        Editor securePrefs = new SecurePreferences(context.getApplicationContext()).edit();
        securePrefs.putString("authorization", serialize());
        securePrefs.commit();

        SharedPreferences.Editor editor = context.getSharedPreferences("GITHUB_APP", Context.MODE_PRIVATE).edit();
        editor.putBoolean("tokenPresent", true);
        editor.commit();
    }

    public static GHAuthorization fetch(Context context) {
        SharedPreferences securePrefs = new SecurePreferences(context.getApplicationContext());
        return create(securePrefs.getString("authorization", ""));
    }

    static public GHAuthorization create(String serializedData) {
        Gson gson = new Gson();
        return gson.fromJson(serializedData, GHAuthorization.class);
    }

    private static class App {
        private String url;
        private String name;
        private String clientId;
    }
}
