package com.afzaln.githublib;

import org.apache.http.HttpStatus;

import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by afzal on 15-02-02.
 */
public class AuthApi {

    private static final String TAG = AuthApi.class.getSimpleName();

    /**
     * Github instance
     */
    Github mGithub;
    boolean useTwoFaToken = false;

    /**
     * Callback declarations, not initialized because
     * they need a non-null mGithub reference in constructor
     */
    private GithubCallback<GHAuthorization> tokenCallback;
    private GithubCallback<User> loginCallback;

    /**
     * Instantiates a new Auth api.
     *
     * @param github the parent github instance
     */
    AuthApi(Github github) {
        mGithub = github;
        useTwoFaToken = (github.twoFaToken != null);
        initCallbacks();
    }

    /**
     * Retrofit interface
     */
    interface LoginService {

        /**
         * User auth endpoint
         *
         * @param callback the callback that gets a {@link User} object
         */
        @GET("/user")
        void userAuth(GithubCallback<User> callback);
        /**
         * Create token endpoint
         *
         * @param authorization authorization object containing all the details
         * @param callback      the callback returning the resulting {@link GHAuthorization} object
         */
        @POST("/authorizations")
        void createToken(@Body GHAuthorization authorization, GithubCallback<GHAuthorization> callback);

    }

    /**
     * Create token.
     *
     * @param authorization {@link GHAuthorization} object containing all the details for authorization
     */
    public void createToken(GHAuthorization authorization) {
        LoginService service = buildNetService();
        service.createToken(authorization, tokenCallback);
    }

    /**
     * Uses the username, password and
     * (optional) two-factor authentication token
     * to login the user
     *
     * @param useTwoFaToken whether to use the two-FA token or not
     */
    public void userLogin() {
        LoginService service = buildNetService();
        service.userAuth(loginCallback);
    }

    /**
     * Builds {@link LoginService} instance
     * for network calls
     *
     * @param interceptor the interceptor containing necessary headers
     * @return the created LoginService instance
     */
    public LoginService buildNetService() {
        RequestInterceptor interceptor = createRequestInterceptor();
        RestAdapter adapter = NetUtils.buildNetService(interceptor);
        return adapter.create(LoginService.class);
    }

    /**
     * Creates a {@link retrofit.RequestInterceptor} with
     * the Authorization header and (if available) the two-FA token
     *
     * @return the created {@link retrofit.RequestInterceptor} instance
     */
    private RequestInterceptor createRequestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                if (mGithub.authorization != null) {
                    request.addHeader("Authorization", "token " + mGithub.authorization.token);
                } else {
                    request.addHeader("Authorization", mGithub.encodedAuthorization);
                    if (useTwoFaToken) {
                        request.addHeader("X-GitHub-OTP", mGithub.twoFaToken);
                    }
                }
                request.addHeader("Accept", "application/vnd.github.mirage-preview+json");
            }
        };
    }

    /**
     * Initialize all callbacks
     */
    private void initCallbacks() {
        tokenCallback = new GithubCallback<GHAuthorization>(mGithub) {
            @Override
            public void success(GHAuthorization authorization, Response response) {
                Log.d(TAG, "token: " + authorization.token);
                authorization.save(mGithub.appContext);
                mGithub.updateLoginState(LoginState.TOKEN_CREATED);
            }
        };

        loginCallback = new GithubCallback<User>(mGithub) {
            @Override
            public void success(User user, Response response) {
                if (response.getStatus() == HttpStatus.SC_OK) {
                    mGithub.currentUser = user;
                    if (mGithub.authorization != null && mGithub.authorization.hashedToken != null) {
                        mGithub.updateLoginState(LoginState.LOGGED_IN_WITH_TOKEN);
                    } else {
                        mGithub.updateLoginState(LoginState.LOGGED_IN);
                    }
                }
            }
        };
    }
}
