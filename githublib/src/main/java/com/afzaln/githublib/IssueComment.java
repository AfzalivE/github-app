package com.afzaln.githublib;

/**
 * Created by afzal on 15-02-04.
 */
public class IssueComment {
    public long id;
    public String url;
    public String htmlUrl;
    public String body;
    public User user;
    public String createdAt;
    public String updatedAt;
}
