package com.afzaln.githublib;

/**
* Created by afzal on 15-02-15.
*/
public class PullRequest {
    public int id;
    public String url;
    public String htmlUrl;
    public String diffUrl;
    public String patchUrl;
    public String state;
    public String title;
    public String body;
    public String createdAt;
    public String updatedAt;
    public String closedAt;
    public String mergedAt;
    public boolean merged;
}
