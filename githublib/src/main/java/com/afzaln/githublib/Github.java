package com.afzaln.githublib;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by afzal on 15-02-01.
 */
public class Github {

    static Github singleton = null;
    Context appContext;

    String encodedAuthorization;

    LoginState loginState;
    LoginStateListener loginStateListener;
    String twoFaToken;

    GHAuthorization authorization;
    public User currentUser;

    public static Github get() {
        if (singleton != null) {
            return singleton;
        }

        throw new IllegalStateException("Github instance is not instantiated, call connect() to do so");
    }

    public Github(Context context, GHAuthorization authorization, LoginStateListener loginStateListener) {
        if (context == null) {
            throw new NullPointerException("Context cannot be null");
        }

        this.appContext = context.getApplicationContext();
        this.authorization = authorization;

        if (loginStateListener != null) {
            this.loginStateListener = loginStateListener;
        }

        new AuthApi(this).userLogin();
    }

    public Github(Context context, String login, String password, LoginStateListener loginStateListener) {
        if (context == null) {
            throw new NullPointerException("Context cannot be null");
        }

        this.appContext = context.getApplicationContext();

        if (password != null) {
            String authorization = login + ":" + password;
            encodedAuthorization = "Basic " + Base64.encodeToString(authorization.getBytes(), Base64.NO_WRAP);
        } else {
            // anonymous access
            encodedAuthorization = null;
            loginState = LoginState.ANONYMOUS;
        }

        if (loginStateListener != null) {
            this.loginStateListener = loginStateListener;
        }

        if (login != null) {
            new AuthApi(this).userLogin();
        }
    }

    public void setLoginStateListener(LoginStateListener listener) {
        this.loginStateListener = listener;
    }

    public static Github connect(Context context, String login, String password, LoginStateListener listener) {
        singleton = new GithubBuilder().withPassword(context, login, password, listener).build();
        return singleton;
    }

    public static Github connect(Context context, LoginStateListener loginStateListener) {
        singleton = new GithubBuilder().withOauthToken(context, loginStateListener).build();
        return singleton;
    }

    void updateLoginState(LoginState state) {
        loginState = state;
        loginStateListener.onLoginStateChanged(state);
    }

    public void createToken(String scopes, String note, String noteUrl, String clientId, String clientSecret) {
        GHAuthorization authorization = new GHAuthorization(this, scopes, note, noteUrl, clientId, clientSecret);
        authorization.authorize();
    }

    public void submitTwoFaToken(String twoFaToken) {
        this.twoFaToken = twoFaToken;
        new AuthApi(this).userLogin();
    }

    public static boolean isTokenPresent(Context context) {
        SharedPreferences editor = context.getApplicationContext().getSharedPreferences("GITHUB_APP", Context.MODE_PRIVATE);
        return editor.getBoolean("tokenPresent", false);
    }

    public EventsApi events() {
        return new EventsApi(this);
    }

}
