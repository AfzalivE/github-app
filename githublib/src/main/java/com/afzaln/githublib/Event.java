package com.afzaln.githublib;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import static com.afzaln.githublib.NetUtils.sGson;

/**
 * Created by afzal on 15-02-04.
 */
public class Event {

    public String id;
    public String type;
    @SerializedName("public")
    public boolean isPublic;
    public Repo repo;
    public Actor actor;
    public Org org;
    public String createdAt;
    public JsonElement payload;

    public EventType getType() {
        String typeString = type;
        if (type.endsWith("Event")) {
            typeString = typeString.substring(0, typeString.length() - 5);
        }
        for (EventType eventPayload : EventType.values()) {
            if (eventPayload.name().replace("_", "").equalsIgnoreCase(typeString)) {
                return eventPayload;
            }
        }
        return null;
    }

    public <T extends EventPayload> T getPayload(Class<T> type) {
        T deserializedPayload = sGson.fromJson(payload, type);
        return deserializedPayload;
    }

    public static class Repo {
        public long id;
        public String name;
        public String url;
    }

    public static class Actor {
        public long id;
        public String login;
        public String gravatarId;
        public String url;
        public String avatarUrl;
    }

    public static class Org extends Actor {}
}
