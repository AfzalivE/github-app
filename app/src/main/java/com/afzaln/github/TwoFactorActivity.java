package com.afzaln.github;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afzaln.githublib.Github;
import com.afzaln.githublib.LoginState;
import com.afzaln.githublib.LoginStateListener;

/**
 * Created by afzal on 14-12-14.
 */
public class TwoFactorActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initialize toolbar
        super.onCreate(savedInstanceState);

        setContentFragment(new TwoFactorFragment());
    }

    public static class TwoFactorFragment extends Fragment {

        private OnClickListener cancelClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        };

        private LoginStateListener mLoginStateListener = new LoginStateListener() {
            @Override
            public void onLoginStateChanged(LoginState newState) {
                switch (newState){
                    case NEEDS_TWO_FA:
                        // bad two fa code
                        break;
                    case LOGGED_IN:
                        // create or get token
                        String scopes = getResources().getString(R.string.scopes);
                        String note = getResources().getString(R.string.note);
                        String clientId = getResources().getString(R.string.client_id);
                        String clientSecret = getResources().getString(R.string.client_secret);

                        mGithub.createToken(scopes, note, "", clientId, clientSecret);
                        break;
                    case TOKEN_CREATED:
                        // token created, show user the home screen
                        openHomeScreen();
                        break;
                    default:
                        // go back to the login screen
                        getActivity().finish();
                        break;
                }
            }
        };

        private OnClickListener loginClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                // should already be initialized from LoginFragment
                mGithub = Github.get();
                mGithub.setLoginStateListener(mLoginStateListener);
                mGithub.submitTwoFaToken(mEtAuthCode.getText().toString());
            }
        };
        private EditText mEtAuthCode;
        private Github mGithub;

        private void openHomeScreen() {
            Intent intent = new Intent(getActivity(), IntroActivity.class);
            startActivity(intent);
            getActivity().finish();
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_2fa, container, false);

            Button btnCancel = (Button) view.findViewById(R.id.cancel);
            btnCancel.setOnClickListener(cancelClickListener);

            Button btnLogin = (Button) view.findViewById(R.id.login);
            btnLogin.setOnClickListener(loginClickListener);

            mEtAuthCode = (EditText) view.findViewById(R.id.auth_code);

            return view;
        }
    }
}
