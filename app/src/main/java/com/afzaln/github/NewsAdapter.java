package com.afzaln.github;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.afzaln.githublib.Event;
import com.afzaln.githublib.EventPayload;
import com.afzaln.githublib.EventPayload.CreateEvent;
import com.afzaln.githublib.EventPayload.DeleteEvent;
import com.afzaln.githublib.EventPayload.ForkEvent;
import com.afzaln.githublib.EventPayload.IssueCommentEvent;
import com.afzaln.githublib.EventPayload.IssuesEvent;
import com.afzaln.githublib.EventPayload.PullRequestEvent;
import com.afzaln.githublib.EventPayload.PushEvent;

/**
 * Created by afzal on 15-02-08.
 */
public class NewsAdapter extends Adapter<NewsAdapter.ViewHolder> {
    private List<Event> mEventList;
    private final Context mContext;
    private OnItemClickListener mItemClickListener;

    public static DateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public NewsAdapter(Context context, List<Event> events) {
        mContext = context;
        mEventList = events;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_news, parent, false);
        ViewHolder vh = new ViewHolder(v, this);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Event event = mEventList.get(position);

        String refType;

        switch (event.type) {
            case "CreateEvent":
                CreateEvent createEvent = event.getPayload(CreateEvent.class);
                viewHolder.mIcon.setImageResource(R.drawable.git_branch);
                viewHolder.mTitle.setText("Created by " + event.actor.login);

                refType = createEvent.refType.substring(0, 1).toUpperCase() + createEvent.refType.substring(1);
                viewHolder.mSubtitle.setText(refType + " " + createEvent.ref + " at " + event.repo.name);

                viewHolder.setDate(event);
                break;
            case "DeleteEvent":
                DeleteEvent deleteEvent = event.getPayload(DeleteEvent.class);
                viewHolder.mIcon.setImageResource(R.drawable.git_branch);
                viewHolder.mTitle.setText("Deleted by " + event.actor.login);

                refType = deleteEvent.refType.substring(0, 1).toUpperCase() + deleteEvent.refType.substring(1);
                viewHolder.mSubtitle.setText(refType + " " + deleteEvent.ref + " at " + event.repo.name);

                viewHolder.setDate(event);
                break;
            case "ForkEvent":
                ForkEvent forkPayload = event.getPayload(ForkEvent.class);
                viewHolder.mIcon.setImageResource(R.drawable.repo_forked);
                viewHolder.mTitle.setText("Forked by " + event.actor.login);
                viewHolder.mSubtitle.setText(event.repo.name);
                break;
            case "PullRequestEvent":
                PullRequestEvent pullRequestEvent = event.getPayload(PullRequestEvent.class);
                viewHolder.mIcon.setImageResource(R.drawable.git_pull_request);

                String action = pullRequestEvent.action;
                if (pullRequestEvent.action.equals("closed")) {
                    if (pullRequestEvent.pullRequest.merged) {
                        action = "merged";
                    }
                }
                viewHolder.mTitle.setText("Pull request " + action + " by " + event.actor.login);
                viewHolder.mSubtitle.setText(event.repo.name + "#" + pullRequestEvent.number);
                viewHolder.setDate(event);
                break;
            case "IssuesEvent":
                IssuesEvent issuePayload = event.getPayload(IssuesEvent.class);
                if (issuePayload.action.equals("closed")) {
                    viewHolder.mIcon.setImageResource(R.drawable.issue_closed);
                } else if (issuePayload.action.equals("reopened")) {
                    viewHolder.mIcon.setImageResource(R.drawable.issue_reopened);
                } else {
                    viewHolder.mIcon.setImageResource(R.drawable.issue_opened);
                }

                viewHolder.mTitle.setText("Issue " + issuePayload.action + " by " + event.actor.login);
                viewHolder.mSubtitle.setText(event.repo.name + "#" + issuePayload.issue.number);
                viewHolder.setMessage(issuePayload.issue.title);
                viewHolder.setDate(event);

                break;
            case "IssueCommentEvent":
                IssueCommentEvent issueCommentEvent = event.getPayload(IssueCommentEvent.class);
                if (issueCommentEvent.action.equals("created")) {
                    viewHolder.mIcon.setImageResource(R.drawable.comment);
                    viewHolder.mTitle.setText("Comment by " + event.actor.login);
                    viewHolder.mSubtitle.setText(event.repo.name + "#" + issueCommentEvent.issue.number);
                    viewHolder.mMessage.setText(issueCommentEvent.comment.body);
                    viewHolder.setDate(event);
                } else {
                    throw new IllegalStateException("Check Github APIs for IssuesCommentEvent action types. This is not a \"created\" action.");
                }

                break;
            case "WatchEvent":
                viewHolder.mIcon.setImageResource(R.drawable.star);
                viewHolder.mTitle.setText("Starred by " + event.actor.login);
                viewHolder.mSubtitle.setText(event.repo.name);
                viewHolder.mMessage.setVisibility(View.GONE);
                viewHolder.setDate(event);

                break;
            case "PushEvent":
                viewHolder.mIcon.setImageResource(R.drawable.git_commit);
                viewHolder.mTitle.setText("Pushed by " + event.actor.login);
                viewHolder.mSubtitle.setText(event.repo.name);

                PushEvent pushPayload = event.getPayload(EventPayload.PushEvent.class);
                viewHolder.setMessage(pushPayload.commits.get(0).message);

                viewHolder.setDate(event);

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return mEventList.get(position).getType().ordinal();
    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void notifyDataSetChanged(List<Event> events) {
        mEventList = events;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final NewsAdapter mAdapter;
        private final ImageView mIcon;
        private final TextView mTitle;
        private final TextView mSubtitle;
        private final TextView mMessage;
        private final TextView mTimestamp;

        public ViewHolder(View root, NewsAdapter adapter) {
            super(root);
            itemView.setOnClickListener(this);
            mAdapter = adapter;
            mTitle = (TextView) root.findViewById(R.id.title);
            mSubtitle = (TextView) root.findViewById(R.id.subtitle);
            mIcon = (ImageView) root.findViewById(R.id.icon);
            mTimestamp = (TextView) root.findViewById(R.id.timestamp);
            mMessage = (TextView) root.findViewById(R.id.message);
        }

        public void setDate(Event event) {
            try {
                CharSequence timestamp = DateUtils.getRelativeTimeSpanString(timeFormatter.parse(event.createdAt).getTime(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);
                mTimestamp.setText(timestamp);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            mAdapter.onItemHolderClick(this);
        }

        public void setMessage(String message) {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }
    }

    private void onItemHolderClick(ViewHolder viewHolder) {
        if (mItemClickListener != null) {
            mItemClickListener.onItemClick(null, viewHolder.itemView,
                    viewHolder.getPosition(), viewHolder.getItemId());
        }
    }
}
