package com.afzaln.github;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;

import com.afzaln.githublib.Event;
import com.afzaln.githublib.Github;
import com.afzaln.githublib.GithubCallback;
import com.afzaln.githublib.LoginState;
import com.afzaln.githublib.LoginStateListener;
import retrofit.client.Response;

/**
 * Created by afzal on 15-02-04.
 */
public class HomeFragment extends Fragment {
    private static final String TAG = HomeFragment.class.getSimpleName();

    private Github mGithub;
    private boolean mLoggedIn;

    private GithubCallback<List<Event>> mEventsCallback;
    private LoginStateListener mLoginStateListener = new LoginStateListener() {
        @Override
        public void onLoginStateChanged(LoginState newState) {
            switch (newState) {
                case LOGGED_IN_WITH_TOKEN:
                    // logged in with token
                    // Populate views
                    mLoggedIn = true;
                    mGithub.events().receivedEvents(mEventsCallback);
                    break;
                default:
                    Log.d(TAG, "Bad login state: " + newState.name());
                    Intent intent = new Intent(getActivity(), IntroActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                    break;
            }
        }
    };
    private MyRecyclerView mNewsListView;
    private NewsAdapter mAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private OnRefreshListener mRefreshNewsListener = new OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (mLoggedIn) {
                mGithub.events().receivedEvents(mEventsCallback);
            } else {
                mGithub = Github.connect(getActivity(), mLoginStateListener);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mGithub = Github.connect(getActivity(), mLoginStateListener);

        initCallbacks();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mNewsListView = (MyRecyclerView) getView().findViewById(R.id.news_list);
//        mNewsListView.setEmptyView(getView().findViewById(R.id.empty));
        mNewsListView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        mNewsListView.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        mNewsListView.setLayoutManager(layoutManager);

        mAdapter = new NewsAdapter(getActivity(), new ArrayList<Event>());
        mAdapter.setItemClickListener(mNewsClickListener);
        mNewsListView.setAdapter(mAdapter);

        mRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        mRefreshLayout.setOnRefreshListener(mRefreshNewsListener);
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
            }
        });
    }

    private void initCallbacks() {
        mEventsCallback = new GithubCallback<List<Event>>(mGithub) {
            @Override
            public void success(List<Event> events, Response response) {
                Log.d(TAG, "Events fetched: " + events.size());
                mRefreshLayout.setRefreshing(false);
                for (Event event : events) {
                    Log.d(TAG, "Watch: " + event.type + ", " + event.actor.login);
                }
                populateViews(events);
            }
        };
    }

    private OnItemClickListener mNewsClickListener;

    private void populateViews(List<Event> events) {
        mAdapter.notifyDataSetChanged(events);
    }
}
