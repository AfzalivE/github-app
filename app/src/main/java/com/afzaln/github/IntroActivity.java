package com.afzaln.github;

import android.os.Bundle;

import com.afzaln.githublib.Github;

/**
 * Created by afzal on 15-02-04.
 */
public class IntroActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initialize toolbar
        super.onCreate(savedInstanceState);

        // show the home screen if token is present
        // otherwise the login screen
        if (Github.isTokenPresent(this)) {
            setContentFragment(new HomeFragment());
        } else {
            setContentFragment(new LoginFragment());
        }
    }
}
