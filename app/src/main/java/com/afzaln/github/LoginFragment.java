package com.afzaln.github;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afzaln.githublib.Github;
import com.afzaln.githublib.LoginState;
import com.afzaln.githublib.LoginStateListener;

public class LoginFragment extends Fragment {

    private static final String TAG = LoginFragment.class.getSimpleName();

    private LoginStateListener mLoginStateListener = new LoginStateListener() {
        @Override
        public void onLoginStateChanged(LoginState newState) {
            Intent intent;
            switch (newState) {
                case BAD_CREDENTIALS:
                    // show bad credentials message
                    break;
                case LOGGED_IN:
                    // create or get token
                    String scopes = getResources().getString(R.string.scopes);
                    String note = getResources().getString(R.string.note);
                    String clientId = getResources().getString(R.string.client_id);
                    String clientSecret = getResources().getString(R.string.client_secret);

                    mGithub.createToken(scopes, note, "", clientId, clientSecret);
                    break;
                case LOGGED_IN_WITH_TOKEN:
                    // logged in with token (this should not occur in this fragment)
                    break;
                case TOKEN_CREATED:
                    // token created, show user the home screen
                    openHomeActivity();
                    break;
                case NEEDS_TWO_FA:
                    // try github login with 2fa token
                    // show 2fa activity
                    intent = new Intent(getActivity(), TwoFactorActivity.class);
                    getActivity().startActivity(intent);
                    break;
                case MAX_ATTEMPTS_EXCEEDED:
                    // show max attempts exceeded message
                    break;
            }
            Log.d(TAG, "Login state: " + newState.name());
        }
    };

    private void openHomeActivity() {
        Intent intent = new Intent(getActivity(), IntroActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private OnClickListener loginClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mGithub = Github.connect(getActivity(), mEtUsername.getText().toString(), mEtPassword.getText().toString(), mLoginStateListener);
        }
    };

    // TODO do sign up flow if possible
    private OnClickListener signupClickListener;
    private EditText mEtUsername;
    private EditText mEtPassword;
    private Github mGithub;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button btnLogin = (Button) view.findViewById(R.id.login);
        btnLogin.setOnClickListener(loginClickListener);

        Button btnSignup = (Button) view.findViewById(R.id.signup);
        btnSignup.setOnClickListener(signupClickListener);

        mEtUsername = (EditText) view.findViewById(R.id.username);
        mEtPassword = (EditText) view.findViewById(R.id.password);

        return view;
    }
}
